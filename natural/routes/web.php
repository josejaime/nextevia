<?php

use App\Mail\ContactMail;
use App\Mail\BusinessMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('layout.main');
    return view('home');
})->name('home');
Route::get('/nutricion', function () {
    return view('nutrition');
})->name('nutricion');
Route::get('/stevia', function () {
    return view('stevia');
})->name('stevia');
Route::get('/que-es', function () {
    return view('que_es');
})->name('que_es');
Route::get('/quees', function () {
    return view('que_es');
})->name('que_es');
Route::get('/donde-comprar', function () {
    return view('donde');
})->name('donde');
Route::get('/contacto', function () {
    return view('contacto');
})->name('contacto');
Route::get('/productos', function () {
    return view('productos');
})->name('productos');

Route::post('/contacto', function (Request $request) {

    $token = $request->token;
    $action = $request->action;

    // call curl to POST request
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => "6Lfza2MaAAAAAMuKRsB8XJsxBx93zE5dH0jJMr6a", 'response' => $token)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    $arrResponse = json_decode($response, true);

    if($arrResponse["success"] == '1' && $arrResponse["action"] == $action && $arrResponse["score"] >= 0.5) {
        Mail::send(new ContactMail($request));
        return redirect('/thanks');
    } else {
        return redirect('/contacto');
    }

    // Mail::send(new ContactMail($request));
    // return redirect('/thanks'); //N@tura1.19x
})->name('contacto');

//Negocios
Route::get('/negocios', function () {
    return view('negocios');
})->name('negocios');

Route::post('/negocios', function (Request $request) {
    Mail::send( new BusinessMail($request) );
    return redirect('/thanks');
})->name('negocios-email');

Route::get('/thanks', function(){
    return view('thanks');
});

Route::get('/hoja', function() {
    return view('home');
});
