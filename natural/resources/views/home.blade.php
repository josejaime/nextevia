@extends('layout.main')

@section('title', 'HOME')

@section('content')

<!-- ======================================================= -->
<section class="banner">
    <h1>
        <i class="fas fa-cart-arrow-down"></i>
        Compra Ahora</h1>
    <ul class="drop-down" style="display: none;">
        <li><a target="_blank" href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=directodeweb">Amazon</a></li>
        <li><a target="_blank" href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar">Walmart</a></li>
        <li><a target="_blank" href="https://gnc.com.mx/real-natural-organics-stevia-60paquetes.html">GNC</a></li>
        <li><a target="_blank" href="https://www.heb.com.mx/natural-fit-endulzante-stevia-natural-96-gr-662722.html">HEB</a></li>
        <li><a target="_blank" href="https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=137&ver=mislistas&succFmt=100&criterio=Endulzante+de+stevia+natural+fit#/Endulzante%20de%20stevia%20natural%20fit">La Comer</a></li>
        <li><a target="_blank" href="https://www.merco.mx/">Merco</a></li>
        <li><a target="_blank" href="http://www.superdelnorte.com.mx/">Santovalle</a></li>
        <li><a target="_blank" href="https://www.supermercadossmart.com/">S-Mart</a></li>
        <li><a target="_blank" href="https://www.soriana.com/soriana/es/">Soriana</a></li>
        <li><a target="_blank" href="http://sumesa.com.mx/sucursales.php">SUMESA</a></li>
        <li><a target="_blank" href="https://www.casaley.com.mx/">LEY</a></li>
        <li><a target="_blank" href="https://alsuper.com/buscar?q=natural+fit+">Al Super</a></li>
        <li><a target="_blank" href="http://www.casaayala.mx/">Casa Ayala</a></li>
        <li><a target="_blank" href="https://abarrotescasavargas.mx/">Casa Vargas</a></li>
    </ul>
</section>
<section id="home-slider" class="main-section">
    <div class="owl-carousel">
        <div class="slide" style="background-image: url('{{ asset('img/la_mejor_stevia.jpg') }}');">
            <a target="_blank" href="https://www.guiadesuplementos.mx/stevia/?fbclid=IwAR3al7l-aN_x5_RJ6T7UQRTQTvHEXcFta2r4nB2WlU6if6uRopnW7pfG5KE"><img src="{{ asset('img/la_mejor_stevia.jpg') }}" alt=""></a>
        </div>
        {{-- <div class="slide" style="background-image: url('{{ asset('img/banner-web-nat-fit-1920x1080-agosto.jpg') }}');">
            <a href="#!"><img src="{{ asset('img/banner-web-nat-fit-1920x1080-agosto.jpg') }}" alt=""></a>
        </div> --}}
        <div class="slide" style="background-image: url('{{ asset('img/slide_pouch.jpg') }}');">
            <a href="https://www.amazon.com.mx/stores/page/373DB61C-906A-44D1-865C-517784BCAC6C?channel=pouch"><img src="{{ asset('img/slide_pouch.jpg') }}" alt=""></a>
        </div>
        <div class="slide" style="background-image: url('{{ asset('img/natural_slide_5.jpg') }}');">
            <a href="https://gnc.com.mx/real-natural-organics-stevia-60paquetes.html" target="_blank">
                <img src="{{ asset('img/natural_slide_5.jpg') }}" alt="">
            </a>
        </div>
        {{-- <div class="slide" style="background-image: url('{{ asset('img/natural_slide_0.jpg') }}');">
            <a href="#!" id="video2020"><img src="{{ asset('img/natural_slide_0.jpg') }}" alt=""></a>
        </div> --}}
        <div class="slide" style="background-image: url('{{ asset('img/natural_slide_1.jpg') }}');">
            <a href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar" target="_blank"><img src="{{ asset('img/natural_slide_1.jpg') }}" alt=""></a>
        </div>
        <div class="slide" style="background-image: url('{{ asset('img/natural_slide_2.jpg') }}');">
            <a href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=directodeweb" target="_blank"><img src="{{ asset('img/natural_slide_2.jpg') }}" alt=""></a>
        </div>
        <div class="slide" style="background-image: url('{{ asset('img/natural_slide_3.jpg') }}');">
            <img src="{{ asset('img/natural_slide_3.jpg') }}" alt="">
        </div>
        {{-- <div class="slide" style="background-image: url('');">
            <img src="{{ asset('img/slide_2.png') }}" alt="">
        </div>
        <div class="slide" style="background-image: url('');">
            <img src="{{ asset('img/slide_3.png') }}" alt="">
        </div> --}}
    </div>
</section>
<section id="thirds" data-aos="fade-up">

    {{-- <div class="third" data-aos="flip-down" style="background:#e5a927 url('{{ asset('img/30-sobres-left.png') }}');"> --}}
    <div class="third" data-aos="flip-down" style="background:#e5a927">
        <img src="{{ asset('img/30-sobres.png') }}" alt="" style="width: 100%;">

        <div class="title">
            <h4 class="light">Caja de </h4>
            <h4>30 sobres</h4>
            {{-- <a href="#!" class="button">Comprar</a> --}}
        </div>
    </div>
    {{-- <div class="third" data-aos="flip-up" style="background:#f1de80 url('{{ asset('img/60-sobres-left.png') }}'); background-size: 60% !important; background-position: left !important;"> --}}
    <div class="third" data-aos="flip-up" style="background:#f1de80;">
        <img src="{{ asset('img/60-sobres.png') }}" alt="" style="margin-top: 5%;">
        <div class="title">
            <h4 class="light">Caja de </h4>
            <h4>60 sobres</h4>
            {{-- <a href="#!" class="button">Comprar</a> --}}
        </div>
    </div>
    <div class="third" data-aos="flip-down" style="background:#afc265 url('{{ asset('img/empaque_120.png') }}');">
    {{-- <div class="third" data-aos="flip-down" style="background:#afc265;">
        <img src="{{ asset('img/120-sobres.png') }}" alt=""> --}}
        <div class="title">
            <h4 class="light">Caja de </h4>
            <h4>120 sobres</h4>
            {{-- <a href="#!" class="button">Comprar</a> --}}
        </div>
    </div>

</section>
<section id="video">

    <video muted="" loop="" controls poster="{{ asset('img/poster.PNG') }}" playsinline="">
		<source src="{{ asset('video/natural_video.mp4') }}" type="video/mp4">
		{{-- <source src="video/slider-banner_1.webm" type="video/webm">
		<source src="video/slider-banner_1.ogv" type="video/ogg"> --}}
	</video>

    <div class="content">
        <div class="up">
            <h2>
                <big>0 Azúcar,</big>
            </h2>
        </div>
        <div class="down">
            <h2>Más Stevia</h2>
        </div>
    </div>

</section>
<section id="productos" data-aos="fade-up">
    <div class="title">
        <div class="behind">
            <img src="{{ asset('img/left.png') }}" alt="">
            <h1>Productos</h1>
            <img src="{{ asset('img/right.png') }}" alt="">
        </div>
        <h1 class="front">Nuestros Productos</h1>
    </div>
    <div id="productos-halfs">

        {{-- <div class="producto-slider">
            <div class="owl-carousel2">
                <div class="slide">
                    <img src="{{ asset('img/50-sobres.png') }}" alt="">

                    <div class="subtitle">
                        <h3>50 Sobres</h3>
                        <a href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=presentaciones" target="_blank" class="button cancan">Comprar</a>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="squares">
            <span>
                <img class="lazy" data-src="{{ asset('img/Bolsa-NatFit-250grs.png') }}">
                <div class="subtitle">
                    <h4>250grs<br> Bolsa</h4>
                    <a href="https://www.amazon.com.mx/stores/page/373DB61C-906A-44D1-865C-517784BCAC6C?channel=pouch" target="_blank" class="button cancan">Comprar</a>
                </div>
            </span>
            <span>
                <img class="lazy" data-src="{{ asset('img/100-sobres.png') }}">
                <div class="subtitle">
                    <h4>50<br> Sobres</h4>
                    <a href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=presentaciones" target="_blank" class="button cancan">Comprar</a>
                </div>
            </span>
            <span>
                <img class="lazy" data-src="{{ asset('img/100-sobres.png') }}">
                <div class="subtitle">
                    <h4>100<br> Sobres</h4>
                    <a href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=presentaciones" target="_blank" class="button cancan">Comprar</a>
                </div>
            </span>
            <span>
                <img class="lazy" data-src="{{ asset('img/empaque_360_side.png') }}">
                <div class="subtitle">
                    <h4>360<br> Sobres</h4>
                    <a href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=presentaciones" target="_blank" class="button cancan">Comprar</a>
                </div>
            </span>
            <span>
                <img class="lazy" data-src="{{ asset('img/1000_side.png') }}">
                <div class="subtitle">
                    <h4>1000<br> Sobres</h4>
                    <a href="https://www.amazon.com.mx/stores/page/1BC0509B-F32E-4908-9D00-2032CCF0B065?channel=mayoreo" target="_blank" class="button cancan">Comprar</a>
                </div>
            </span>
            <span>
                <img class="lazy" data-src="{{ asset('img/1000_side.png') }}">
                <div class="subtitle">
                    <h4>12 Cajas<br>de 120 Sobres</h4>
                    <a href="https://www.amazon.com.mx/stores/page/1BC0509B-F32E-4908-9D00-2032CCF0B065?channel=mayoreo" target="_blank" class="button cancan">Comprar</a>
                </div>
            </span>
        </div>

    </div>
</section>
<section id="buy" data-aos="fade-up">
    <div class="title">
        <h1>Compra en</h1>
    </div>
    <div class="">
        <div class="prev-nav">
            <a href="#!" class="prev"><i class="fas fa-3x fa-chevron-left"></i></a>
        </div>
        <div class="buy-slider owl-carousel">
            <div class="slide">
                <a target="_blank" href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=directodeweb"><img src="{{ asset('img/amazon.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar"><img src="{{ asset('img/walmart.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://gnc.com.mx/real-natural-organics-stevia-60paquetes.html"><img src="{{ asset('img/gnc.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/grand.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.heb.com.mx/natural-fit-endulzante-stevia-natural-96-gr-662722.html"><img src="{{ asset('img/heb.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href=" https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=137&ver=mislistas&succFmt=100&criterio=Endulzante+de+stevia+natural+fit#/Endulzante%20de%20stevia%20natural%20fit"><img src="{{ asset('img/logo-la-comer.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.merco.mx/"><img src="{{ asset('img/merco.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://www.superdelnorte.com.mx/"><img src="{{ asset('img/santovalle.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.supermercadossmart.com/"><img src="{{ asset('img/smart-logo.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.soriana.com/soriana/es/"><img src="{{ asset('img/soriana.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://sumesa.com.mx/sucursales.php"><img src="{{ asset('img/sumesa.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/vimark.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.casaley.com.mx/"><img src="{{ asset('img/ley.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://alsuper.com/buscar?q=natural+fit+"><img src="{{ asset('img/al-super.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/avila.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://www.casaayala.mx/#"><img src="{{ asset('img/casa-ayala.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://abarrotescasavargas.mx/"><img src="{{ asset('img/casa-vargas.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=406&ver=mislistas&succFmt=100&criterio=natural+fit#/natural%20fit"><img src="{{ asset('img/logo-fresko.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/san-borja.png') }}" alt=""></a>
            </div>
        </div>
        <div class="next-nav">

            <a href="#!" class="next"><i class="fas fa-3x fa-chevron-right"></i></a>
        </div>
    </div>
</section>
<section id="people">
    <div class="part text">
        <h2>La Stevia</h2>
        {{-- <h3>Todos</h3> --}}
    </div>
    <div class="part" style="background-image: url('{{ asset('img/side_girl.jpg')}}'); border-right: 1px solid white;"></div>
    <div class="part" style="background-image: url('{{ asset('img/side_man.jpg')}}')"></div>
</section>
<section id="recetas">
    <div class="title">
        <div class="behind">
            <img src="{{ asset('img/left_1.png') }}" alt="">
            <h1>Blog</h1>
            <img src="{{ asset('img/right_1.png') }}" alt="">
        </div>
        <h1 class="front">Recetas</h1>
    </div>
    <div style="margin: 0 auto;">
        <div id="recepies" class="owl-carousel">
            <a href="#!" class="recipe" id="receta1" onclick="loadReceta(15)">
                <div>
                    <img src="{{ asset('img/receta_15.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Agua de Pepino</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta1" onclick="loadReceta(16)">
                <div>
                    <img src="{{ asset('img/receta_16.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Cheesecake</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta1" onclick="loadReceta(17)">
                <div>
                    <img src="{{ asset('img/receta_17.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Pastel Integral</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta1" onclick="loadReceta(18)">
                <div>
                    <img src="{{ asset('img/receta_18.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Tarta de Manzana</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta1" onclick="loadReceta(1)">
                <div>
                    <img src="{{ asset('img/receta_1.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Agua de piña, apio y zanahoria</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta2" onclick="loadReceta(2)">
                <div>
                    <img src="{{ asset('img/receta_2.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Frappé de café</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta3" onclick="loadReceta(3)">
                <div>
                    <img src="{{ asset('img/receta_3.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Agua de fresa con hierbabuena</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta4" onclick="loadReceta(4)">
                <div>
                    <img src="{{ asset('img/receta_4.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Mousse de limón</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta5" onclick="loadReceta(5)">
                <div>
                    <img src="{{ asset('img/receta_5.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Leche de Linaza</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta6" onclick="loadReceta(6)">
                <div>
                    <img src="{{ asset('img/receta_6.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Trufas de chocolate</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta7" onclick="loadReceta(7)">
                <div>
                    <img src="{{ asset('img/receta_7.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Batido de fresa y nuéz</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta8" onclick="loadReceta(8)">
                <div>
                    <img src="{{ asset('img/receta_8.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Mantequilla de maní natural</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta9" onclick="loadReceta(9)">
                <div>
                    <img src="{{ asset('img/receta_9.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Hot cakes de avena</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta10" onclick="loadReceta(10)">
                <div>
                    <img src="{{ asset('img/receta_10.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Galletas de avena</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta11" onclick="loadReceta(11)">
                <div>
                    <img src="{{ asset('img/receta_11.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Gelatina de queso con fresa</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta12" onclick="loadReceta(12)">
                <div>
                    <img src="{{ asset('img/receta_12.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Batido de mango y arándano</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta13" onclick="loadReceta(13)">
                <div>
                    <img src="{{ asset('img/receta_13.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Paletas de fruta</h4>

                </div>
            </a>
            <a href="#!" class="recipe" id="receta14" onclick="loadReceta(14)">
                <div>
                    <img src="{{ asset('img/receta_14.jpg') }}" alt="">
                </div>
                <div class="content">
                    {{-- <span><i>Noviembre 19, 2019</i></span> --}}
                    <h4>Red velvet sin horno</h4>

                </div>
            </a>
        </div>
        <div class="recepiesnav" style="text-align:center;">
            <div class="prev-nav" style="float: left; width: 50%; text-align: center;">
                <a href="#!" class="prev"><i style="color: #6ea485 !important;" class="fas fa-3x fa-chevron-left"></i></a>
            </div>
            <div class="next-nav" style="float: left; width: 50%; text-align: center;">
                <a href="#!" class="next"><i style="color: #6ea485 !important;" class="fas fa-3x fa-chevron-right"></i></a>
            </div>
            <div class="spacer" style="clear:both;"></div>
        </div>

    </div>

</section>
<section>
    <p>&nbsp;</p>
</section>
{{-- <section id="galery">
    <div class="title">
        <div class="behind">
            <img src="{{ asset('img/left_3.png') }}" alt="">
            <h1>Instagram</h1>
            <img src="{{ asset('img/right_3.png') }}" alt="">
        </div>
        <h1 class="front">Nuestra Galería</h1>
    </div>

    <div id="blocks">
        <div class="block">
            <div>
                <img class="lazy" data-src="{{ asset('img/galery_1.jpg') }}" alt="">
            </div>
        </div>
        <div class="block">
            <div>
                <img class="lazy" data-src="{{ asset('img/galery_2.jpg') }}" alt="">
            </div>
        </div>
        <div class="block">
            <div>
                <img class="lazy" data-src="{{ asset('img/galery_3.jpg') }}" alt="">
            </div>
        </div>
        <div class="block">
            <div>
                <img class="lazy" data-src="{{ asset('img/galery_4.jpg') }}" alt="">
            </div>
        </div>
        <div class="block">
            <div>
                <img class="lazy" data-src="{{ asset('img/galery_5.jpg') }}" alt="">
            </div>
        </div>
        <div class="block">
            <div>
                <img class="lazy" data-src="{{ asset('img/galery_6.jpg') }}" alt="">
            </div>
        </div>
        <div class="block">
            <div>
                <img class="lazy" data-src="{{ asset('img/galery_7.jpg') }}" alt="">
            </div>
        </div>
        <div class="block">
            <div>
                <img class="lazy" data-src="{{ asset('img/galery_8.jpg') }}" alt="">
            </div>
        </div>
    </div>

</section> --}}

<!-- ======================================================* -->
<div class="ui basic modal" id="modal1">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235945847?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235945847?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        {{-- <iframe width="640" height="360" data-loader="iframe" data-src="https://www.youtube.com/embed/mE6t2mFcdP8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
    </div>
</div>
<div class="ui basic modal" id="modal2">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235946025?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235946025?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal3">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235946138?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235946138?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal4">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235946234?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235946234?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal5">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235946415?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235946415?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal6">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235946509?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235946509?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal7">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235946688?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235946688?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal8">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235946824?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235946824?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal9">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235946936?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235946936?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>

<div class="ui basic modal" id="modal10">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235947395?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235947395?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal11">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235947609?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235947609?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal12">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235947693?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235947693?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal13">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235947772?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235947772?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal14">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        {{-- <iframe src="https://player.vimeo.com/video/235962160?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> --}}
        <iframe data-loader="iframe" data-src="https://player.vimeo.com/video/235962160?title=0&byline=0&portrait=0" data-error-detect="true" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal15">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        <iframe width="640" height="360" data-loader="iframe" data-src="https://www.youtube.com/embed/mE6t2mFcdP8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal16">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        <iframe width="640" height="360" data-loader="iframe" data-src="https://www.youtube.com/embed/Z6mekhFCU_Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal17">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        <iframe width="640" height="360" data-loader="iframe" data-src="https://www.youtube.com/embed/IhvR2ywB3G8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
<div class="ui basic modal" id="modal18">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        <iframe width="640" height="360" data-loader="iframe" data-src="https://www.youtube.com/embed/l6CwTkvapXY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
{{-- <div class="ui basic modal" id="modalVideo">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        <video src="{{ asset('video/video_2020.mp4')}}" width="100%" controls id="videoNatural">

        </video>
    </div>
</div> --}}
<div class="ui basic modal" id="modalCompras">
    <div class="ui icon header">
    </div>
    <div class="content" style="text-align: center;">
        <div class="image">
            <img src="{{ asset('img/empaque_360_side.png') }}" alt="Paquete Natural Fit">
        </div>
        <div class="description">
            <div class="title">Natural Fit, Auténtico Endulzante de Stevia de 360 Sobres</div>
            {{-- <span class="price">$199</span> --}}
            <ul>
                <li>100% Natural</li>
                <li>7% de Stevia presente por Sobre</li>
                <li>El único con 100% Stevia Mexicana desde el Cultivo y Elaboración</li>
                <li>Proceso de Extracción Patentado solo a Base de Agua, libre de químicos y solventes</li>
                <li>El doble de contenido de Stevia contra la competencia.</li>
            </ul>
            <div>
                <a href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=presentaciones" target="_blank" class="buy-it">
                    Comprar ahora
                </a>
            </div>
        </div>
    </div>
    <div class="content" style="text-align: center;">
        <div class="image">
            <img src="{{ asset('img/Bolsa-NatFit-250grs.png') }}" alt="Paquete Natural Fit">
        </div>
        <div class="description">
            <div class="title">Natural Fit, Auténtico Endulzante de Stevia Pouch de 250 Gramos</div>
            {{-- <span class="price">$199</span> --}}
            <ul>
                <li>100% Natural con 7% de Stevia por Sobre</li>
                <li>El único con 100% Stevia Mexicana desde el Cultivo y Elaboración</li>
                <li>Proceso de Extracción Patentado solo a Base de Agua, libre de químicos y solventes</li>
                <li>Ideal para Hornear, para Diabéticos, para Dieta Keto. Mejor que la Fruta del Monje mezclada con aditivos químicos</li>
                <li>El doble de contenido de Stevia contra la competencia.</li>
            </ul>
            <div>
                <a href="https://www.amazon.com.mx/stores/page/373DB61C-906A-44D1-865C-517784BCAC6C?channel=pouch" class="buy-it" target="_blank">
                    Comprar ahora
                </a>
            </div>
        </div>
    </div>
    <div class="content" style="text-align: center;">
        <div class="image">
            <img src="{{ asset('img/1000_side.png') }}" alt="Paquete Natural Fit">
        </div>
        <div class="description">
            <div class="title">Natural Fit, Auténtico Endulzante de Stevia de 1000 sobres</div>
            {{-- <span class="price">$199</span> --}}
            <ul>
                <li>100% Natural e ideal para tu Hogar o Negocio</li>
                <li>7% de Stevia presente por Sobre</li>
                <li>100% Stevia Mexicana desde el Cultivo y Elaboración</li>
                <li>Proceso de Extracción Patentado solo a Base de Agua</li>
                <li>El doble de contenido de Stevia contra la competencia.</li>
            </ul>
            <div>
                <a href="https://www.amazon.com.mx/stores/page/1BC0509B-F32E-4908-9D00-2032CCF0B065?channel=mayoreo" target="_blank" class="buy-it">
                    Comprar ahora
                </a>
            </div>
        </div>
    </div>
</div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/home.css?v=2.1') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script>
        $(function() {
           $('.lazy').Lazy();
        });
    </script>
    <script>
        function loadReceta( index ) {
            $('#modal'+index+'.ui.basic.modal')
              .modal('show')
            ;

            if ($('#modal'+index+' iframe').attr('src')) {
                //console.log($('#modal2 iframe').attr('src'));
            }
            else {
                //console.log("Aquí no hay nada");
                $('#modal'+index+' iframe').attr('src', $('#modal'+index+' iframe').attr('data-src'));
            }

        }
        jQuery(function($) {

            $(".banner h1").on('click', function(){
                $(".banner ul").toggle("slow");
            });

            $('.ui.modal')
                .modal()
            ;

            $('#modalVideo.ui.modal')
                .modal({
                    onHide: function(){
                        console.log('hidden');
                        $("#videoNatural").get(0).pause();
                    },
                    onShow: function(){
                        console.log('shown');
                        $("#videoNatural").get(0).play();
                    },
                    onApprove: function() {
                        console.log('Approve');
                        return validateModal()
                    }
                })
            ;
            $('#video2020').on('click', function(){

                $('#modalVideo.ui.basic.modal')
                  .modal('show')
                ;
            });

            $('#home-slider .owl-carousel').owlCarousel({
                loop:true,
                margin:0,
                autoplay: true,
                autoplayTimeout: 7000,
                autoplayHoverPause: true,
                nav:true,
                dots: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });
            $('.buy-slider.owl-carousel').owlCarousel({
                loop: true,
                margin: 20,
                lazyLoad:true,
                nav: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    768: {
                        items:3
                    },
                    1000:{
                        items:3
                    },
                    1365:{
                        items:4
                    }
                }
            });

            $('#recepies.owl-carousel').owlCarousel({
                loop: false,
                margin: 10,
                lazyLoad:false,
                nav: false,
                dots: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    768: {
                        items:3
                    },
                    1000:{
                        items:3
                    },
                    1365:{
                        items:4
                    }
                }
            });


            var owl = $('.producto-slider .owl-carousel');
            $(".producto-slider .next").on('click', function() {
                next();
            });
            $(".producto-slider .prev").on('click', function() {
                prev();
            });

            $("#buy .next").on('click', function() {
                $('.buy-slider.owl-carousel').trigger('prev.owl.carousel');
            });
            $("#buy .prev").on('click', function() {
                $('.buy-slider.owl-carousel').trigger('next.owl.carousel');
            });

            $(".recepiesnav .next").on('click', function() {
                $('#recepies.owl-carousel').trigger('next.owl.carousel');
            });
            $(".recepiesnav .prev").on('click', function() {
                $('#recepies.owl-carousel').trigger('prev.owl.carousel');
            });

            function next(){
                owl.trigger('next.owl.carousel')
            }
            function prev(){
                owl.trigger('next.owl.carousel')
            }

            $(".button").on('click', function(){
                gtag('event', 'conversion', {'send_to': 'AW-815753133/7FYrCIOb7skBEK3P_YQD'});
                //gtag_report_conversion($(this).attr('href'));
                fbq('track', 'Lead');
                gtag('event', 'click', {
                  'event_category': 'amazon',
                  'event_label': 'web'
                });
            });
            $(".button2").on('click', function(){
                fbq('track', 'Lead');
                gtag('event', 'click', {
                  'event_category': 'amazon',
                  'event_label': 'web'
                });
                gtag('event', 'conversion', {'send_to': 'AW-815753133/7FYrCIOb7skBEK3P_YQD'});
                //gtag_report_conversion($(this).attr('href'));
            });
            $('.buy-it').on('click', function() {
                fbq('track', 'Lead');
                gtag('event', 'click', {
                  'event_category': 'amazon',
                  'event_label': 'popup'
                });
                gtag('event', 'conversion', {'send_to': 'AW-815753133/7FYrCIOb7skBEK3P_YQD'});
                //gtag_report_conversion($(this).attr('href'));
            });
        });
        $( document ).ready( function() {
            @if( isset($_GET['utm_source']) )
                $('#modalCompras').modal('show');
            @endif
        });
    </script>

@endpush                                                                                                                                                                                                                                                               
