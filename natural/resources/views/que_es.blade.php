@extends('layout.main')

@section('title', 'HOME')

@section('content')

<!-- ======================================================= -->
<section id="whatis" class="main-section">
    <div class="title">
        <div class="behind">
            <img src="{{ asset('img/left.png') }}" alt="">
            <h1>Stevia</h1>
            <img src="{{ asset('img/right.png') }}" alt="">
        </div>
        <h1 class="front">¿Qué es?</h1>
    </div>
</section>
<section id="video">
    <video preload="auto" muted="" loop="" controls poster="video/poster.png" playsinline="">
		<source src="{{ asset('video/natural_video.mp4') }}" type="video/mp4">
		{{-- <source src="video/slider-banner_1.webm" type="video/webm">
		<source src="video/slider-banner_1.ogv" type="video/ogg"> --}}
	</video>
    <div class="color">

    </div>
</section>
<section id="sobres">
    <img src="{{ asset('img/natural_sobres.png') }}" alt="" data-aos="zoom-in-down">

    <div class="side-title large" data-aos="fade-left">
        <h2>Auténtico</h2>
        <h3>
            Endulzante <br>
            de Stevia
        </h3>
    </div>
    <div class="side-title medium" data-aos="fade-left">
        <h2>Cero</h2>
        <h3>
            Calorías
        </h3>
    </div>
    <div class="side-title tiny" data-aos="fade-left">
        <h2>100%</h2>
        <h3>
            Natural
        </h3>
    </div>
    <div class="spacer" style="clear:both;"></div>
</section>

<section id="dust">
    <img src="{{ asset('img/stevia-polvo.png') }}" alt="" data-aos="zoom-in-down">

    <div class="side-title large" data-aos="fade-right">
        <h2>Stevia</h2>
        <h3>
            100% <br>
            Mexicana
        </h3>
    </div>
    <div class="side-title medium" data-aos="fade-right">
        <h2>Dulce</h2>
        <h3>
            Y Delicioso
        </h3>
    </div>
    <div class="side-title tiny" data-aos="fade-right">
        <h2>Aprobado Por</h2>
        <h3>
            La Asociación<br>
            Mexicana de Diabetes
        </h3>
    </div>

    <div class="spacer" style="clear:both;"></div>
</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/whatis.css?v=2.2') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>

@endpush
