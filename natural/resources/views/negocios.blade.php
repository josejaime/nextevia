@extends('layout.main')

@section('title', 'Contacto')

@section('content')

<!-- ======================================================= -->
<section id="contact" class="main-section">
    <div class="title">
        <div class="behind">
            <img src="{{ asset('img/left.png') }}" alt="">
            <h1>Negocios</h1>
            <img src="{{ asset('img/right.png') }}" alt="">
        </div>
        <h1 class="front">Negocios</h1>
    </div>
</section>
<section id="form">

    <form action="{{ route('negocios-email') }}" class="ui form" method="post" id="businessForm">

        @csrf

        <div class="field">
            <label>Nombre</label>
            <input type="text" name="name" id="name" placeholder="Nombre...">
        </div>

        <div class="field">
            {{-- <label>Upper Fields</label> --}}
            <div class="two fields">
                <div class="field">
                    <label>Télefono</label>
                    <input type="text" name="phone" id="phone" placeholder="Teléfono...">
                </div>
                <div class="field">
                    <label>Correo electrónico</label>
                    <input type="email" name="email" id="email" placeholder="Email...">
                </div>
            </div>
            <div class="field">
                <label>Giro del Negocio</label>
                <select name="type" id="type" class="ui search dropdown">
                    <option value="Cafetería">Cafetería</option>
                    <option value="Restaurante">Restaurante</option>
                    <option value="Hotel">Hotel</option>
                    <option value="Club de Nutrición">Club de Nutrición</option>
                    <option value="Nutriologo">Nutriologo</option>
                </select>
            </div>
            <div class="field">
                <label>Nombre de tu Negocio</label>
                <input type="text" name="business" id="business" placeholder="Nombre del negocio...">
            </div>
            <div class="field">
                <label>Qué tipo de Edulcorante utilizas en tu negocio</label>
                <input type="text" name="edulcorante" id="edulcorante" placeholder="Edulcorante...">
            </div>
            <div class="field">
                <label>Qué cantidad usas mensualmente</label>
                <input type="text" name="quantity" id="quantity" placeholder="Cantidad...">
            </div>
        </div>
        <div class="field">
            <label for="error" id="error"></label>
        </div>
        <div class="field">
            <button class="ui button" type="button" id="send">Enviar</button>
        </div>
    </form>


</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">
        jQuery( document ).ready( function($) {

            $("#send").on('click', function() {

                $("#error").html('');
                $("#send").html('Enviar');

                var ok = true;

                if( ! $('#name').val() ) {
                    ok = false;
                }
                if( ! $('#phone').val() ) {
                    ok = false;
                }
                if( ! $('#email').val() ) {
                    ok = false;
                }
                if( ! $('#business').val() ) {
                    ok = false;
                }
                if( ! $('#edulcorante').val() ) {
                    ok = false;
                }
                if( ! $('#quantity').val() ) {
                    ok = false;
                }

                if( ok ) {
                    //send
                    $("#error").html('');
                    $("#send").html('Enviando...');
                    $('#businessForm').submit();
                }
                else {
                    $("#error").html('*Completa el formulario antes de continuar');
                }

                //return ok;
            });

        });
    </script>

@endpush
