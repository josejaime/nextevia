@extends('layout.main')

@section('title', 'Gracias')

@section('content')

<!-- ======================================================= -->
<style media="screen">
    #thanks {
        display: flex;
        flex-flow: column;
        align-items: center;
        justify-content: center;
        min-height: 400px;
        min-height: 50vh;
    }
</style>
<section id="thanks">

    <h1>Gracias por contactarnos</h1>
    <h3>Nos pondremos en contacto contigo lo más pronto posible</h3>

</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">
        jQuery( document ).ready( function($) {
            
        });
    </script>

@endpush
