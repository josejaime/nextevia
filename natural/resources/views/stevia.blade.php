@extends('layout.main')

@section('title', 'HOME')

@section('content')

<!-- ======================================================= -->
<section id="stevia" class="main-section">
    <div class="title">
        <div class="behind">
            <img src="{{ asset('img/left_1.png') }}" alt="">
            <h1>Planta</h1>
            <img src="{{ asset('img/right_1.png') }}" alt="">
        </div>
        <h1 class="front">Stevia</h1>
    </div>
</section>
<section data-aos="fade-up">
    <h1 class="big cancan">100%</h1>
    <h1 class="big natural">Natural</h1>
</section>
<section id="plant">
    <div class="plant" data-aos="zoom-in-down">
        <img src="{{ asset('img/stevia_planta.png') }}" alt="">
    </div>
    <div class="accordions">
        <a href="#!" class="accordion" data-aos="fade-left">
            <p>
                <i class="fa fa-chevron-left active"></i>Sabor rico y dulce
            </p>
            <span class="content">
                Nuestra Stevia es hasta 300 veces más dulce que el azúcar
            </span>
        </a>
        <a href="#!" class="accordion" data-aos="fade-left"><p><i class="fa fa-chevron-left"></i>¿Qué es?</p>
            <span class="content" style="display:none;">
                La stevia (Stevia rebaudiana Bertoni) es una planta originaria de Paraguay. Contiene cero calorías.
            </span>
        </a>
        <a href="#!" class="accordion" data-aos="fade-left"><p><i class="fa fa-chevron-left"></i>Historia</p>
            <span class="content" style="display:none;">
                Desde hace cientos de años se ha utilizado como endulzante en las comunidades Guaraníes de Sudamérica. En Japón se utiliza desde los años 70´s. En 2011 llega a los supermercados de México.
            </span>
        </a>
        <a href="#!" class="accordion" data-aos="fade-left"><p><i class="fa fa-chevron-left"></i>¿Como saber si es auténtica?</p>
            <span class="content" style="display:none;">
                Las hojas de Stevia están compuestas por componentes dulces y componentes amargos, el secreto está en extraer solo sus componentes dulces. Muchas marcas de Stevia no logran deshacerse de sus componentes amargos en su proceso de extracción lo cual resulta en un sabor amargo, por eso recurren a enmascarar ese mal sabor con otros endulzantes sintéticos y al final resulta una revoltura que es todo menos natural.
            </span>
        </a>
        <a href="#!" class="accordion" data-aos="fade-left"><p><i class="fa fa-chevron-left"></i>Nuestro método</p>
            <span class="content" style="display:none;">
                Tenemos un método exclusivo para extraer el sabor dulce de nuestras hojas de Stevia, usamos solo agua y nada mas lo que proporciona la stevia más pura y deliciosa del mercado. Nosotros cuidamos nuestras plantas desde que están en el campo hasta que extraemos su sabor único e inigualable. Aol consumir Naturalfit estas consumiendo un producto cuidado desde el campo hasta tu mesa.
            </span>
        </a>
    </div>
</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/stevia.css') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">
        $(".accordion").on('click', function() {
            console.log("clicked");
            $(this).children('span').toggle('slow');
            $(this).children('p').children('i').toggleClass('active');
        });
    </script>

@endpush
