@extends('layout.main')

@section('title', 'Contacto')

@section('content')

<!-- ======================================================= -->
<section id="contact" class="main-section">
    <div class="title">
        <div class="behind">
            <img src="{{ asset('img/left.png') }}" alt="">
            <h1>Comunicación</h1>
            <img src="{{ asset('img/right.png') }}" alt="">
        </div>
        <h1 class="front">Contacto</h1>
    </div>
</section>
<section id="form">

    <form action="{{ route('contacto') }}" class="ui form" method="post" id="contactForm">

        @csrf

        <div class="field">
            <label>Nombre</label>
            <input type="text" name="name" placeholder="Nombre..." id="name">
        </div>

        <div class="field">
            {{-- <label>Upper Fields</label> --}}
            <div class="two fields">
                <div class="field">
                    <label>Télefono</label>
                    <input type="text" name="phone" id="phone" placeholder="Teléfono...">
                </div>
                <div class="field">
                    <label>Correo</label>
                    <input type="email" name="email" id="email" placeholder="Email...">
                </div>
                <div class="field">
                    <label>Ciudad</label>
                    <input type="text" name="city" id="city" placeholder="Ciudad...">
                </div>
            </div>
        </div>
        <div class="ui form">
            <div class="field">
                <label>Mensaje</label>
                <textarea name="message" id="message"></textarea>
            </div>
        </div>
        <div class="field">
            <label for="error" id="error"></label>
        </div>
        <div class="field">
            <button class="ui button" type="button" id="send">Enviar</button>
        </div>
    </form>


</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    {{-- <script src="https://www.google.com/recaptcha/api.js"></script> --}}
    <script type="text/javascript">
        jQuery( document ).ready( function($) {

            $("#send").on('click', function() {

                $("#error").html('');
                $("#send").html('Enviar');

                var ok = true;

                if( ! $('#name').val() ) {
                    ok = false;
                }
                if( ! $('#phone').val() ) {
                    ok = false;
                }
                if( ! $('#email').val() ) {
                    ok = false;
                }
                if( ! $('#city').val() ) {
                    ok = false;
                }
                if( ! $('#message').val() ) {
                    ok = false;
                }

                if( ok ) {
                    //send
                    $("#error").html('');
                    $("#send").html('Enviando...');
                    //$('#contactForm').submit();
                    grecaptcha.ready(function() {
                        grecaptcha.execute('6Lfza2MaAAAAACX9o5JvVMYpjADRnXisM4Mq9Ouf', {action: 'send_contact'}).then(function(token) {
                            $('#contactForm').append('<input type="hidden" name="token" value="' + token + '">');
                            $('#contactForm').append('<input type="hidden" name="action" value="send_contact">');
                            //$('#contactForm').unbind('submit').submit();
                            $("#contactForm").submit();
                        });
                    });
                }
                else {
                    $("#error").html('*Completa el formulario antes de continuar');
                }

                //return ok;
            });
        });
    </script>

@endpush
