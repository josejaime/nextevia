@extends('layout.main')

@section('title', 'Nutrición')

@section('content')

<!-- ======================================================= -->
<section id="nutrition" class="main-section">
    <div class="title">
        <div class="behind">
            <img src="{{ asset('img/left_3.png') }}" alt="">
            <h1>Salud</h1>
            <img src="{{ asset('img/right_3.png') }}" alt="">
        </div>
        <h1 class="front">Nutrición</h1>
    </div>
    <div class="image">
        <img src="{{ asset('img/natural_sobre.png') }}" alt="">
    </div>
    <div class="table">
        <div class="title">
            <h1>&nbsp;</h1>
            {{-- <h1>&nbsp;</h1> --}}
        </div>
        <img src="{{asset('img/tabla-comparativa1.png?v=2')}}" alt="">
        <img src="{{asset('img/tabla-comparativa2.png?v=2')}}" alt="">
        {{-- <table>
            <thead>

            </thead>
            <tbody>
                <tr>
                    <td>Contenido energético/Calorías 0KJ(Kcal)</td>
                    <td>
                        Carbohidratos (hidratos de carbono)<br>
                        Carbohidratos totales 0.8g
                    </td>
                </tr>
                <tr>
                    <td>Proteinas 0g</td>
                    <td>
                        Ázucares
                    </td>
                </tr>
                <tr>
                    <td>Grasas (lípidos)/Grasas totales 0g</td>
                    <td>
                        Fibra dietética 0g
                    </td>
                </tr>
                <tr>
                    <td>Grasa Saturada 0g</td>
                    <td>
                        Sodio 0g
                    </td>
                </tr>

            </tbody>
        </table> --}}
    </div>

</section>
<section class="left" id="mister">
    <img src="{{ asset('img/girls_2.png') }}" alt="">
    <div class="content">
        <h2>Es para</h2>
        <h2 class="sand">todos</h2>
    </div>
    <div class="spacer" style="clear:both;"></div>
</section>
<section class="right" id="girls">
    <img src="{{ asset('img/girls.png') }}" alt="">
    <div class="content">

    </div>
    <div class="spacer" style="clear:both;"></div>
</section>
<section id="content">
    <h3>Debido a su cero contenido glicémico, a que es natural y el método de
        extracción solo utiliza agua, <span class="green">Natural Fit</span>
        es recomendable para toda la familia incluyendo <span class="red">niños,
        mujeres embarazadas y diabéticos</span>
    </h3>
</section>
<section>
    {{-- <div class="prev">
        <a href="#!" class="next"><i class="fas fa-3x fa-chevron-left"></i></a>
    </div> --}}
    {{-- <img src="{{ asset('img/inner1.png') }}" alt="" width="100%"> --}}
    <div class="slider owl-carousel" >
        @for ($i=1; $i < 6; $i++)
            <div class="slide">
                <img src="{{ asset('img/inner'.$i.'.png') }}" alt="">
            </div>
        @endfor
    </div>
    {{-- <div class="next">
        <a href="#!" class="next"><i class="fas fa-3x fa-chevron-right"></i></a>
    </div> --}}
</section>
<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/nutrition.css') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">

        $(document).ready( function(){

            $('.slider.owl-carousel').owlCarousel({
                loop:true,
                margin:0,
                nav:true,
                dots: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });
        });
    </script>

@endpush
