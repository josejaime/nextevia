@extends('layout.main')

@section('title', 'Productos')

@section('content')

<!-- ======================================================= -->
<section id="productos" class="main-section">
    
</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/stevia.css') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">
        $(".accordion").on('click', function() {
            console.log("clicked");
            $(this).children('span').toggle('slow');
            $(this).children('p').children('i').toggleClass('active');
        });
    </script>

@endpush
