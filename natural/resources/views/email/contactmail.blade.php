<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            table {
                width: 80%;
                margin: 0 auto;
                /* border: 1px solid; */
            }
            thead {
                padding: 15px;
                background-color: #6ea485;
                color: white;
            }
            thead tr.main-title th {
                padding: 20px 25px;
                text-transform: uppercase;
            }
            thead tr.subtitle th {
                background-color: #7f9f48;
                padding: 10px;
                font-style: italic;
                font-weight: 300;
                color: black;
            }
            tbody tr.subtitle td {
                background-color: #7f9f48;
                padding: 10px;
                font-style: italic;
                font-weight: 300;
                color: black;
                text-align: center;
                border: none !important;
            }
            tbody td:not(.title) {
                padding: 25px;
                border-bottom: 1px solid black;
            }
            tbody .title{
                background-color: #6ea485;
                width: 125px;
                text-align: center;
                font-size: 20px;
            }
        </style>
    </head>
    <body>

        <table>
            <thead>
                <tr class="main-title">
                    <th colspan="12">
                        Mensaje de contacto recibido
                    </th>
                </tr>
                <tr class="subtitle">
                    <th colspan="12">
                        Generado y enviado por <b><a href="https://secuenciadigital.com">Secuencia</a></b>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="title">
                        <b>Nombre:</b>
                    </td>
                    <td>
                        {{ $email->name }}
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        <b>Email:</b>
                    </td>
                    <td>
                        {{ $email->email }}
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        <b>Teléfono:</b>
                    </td>
                    <td>
                        {{ $email->phone }}
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        <b>Ciudad:</b>
                    </td>
                    <td>
                        {{ $email->city }}
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        <b>Mensaje:</b>
                    </td>
                    <td>
                        {{ $email->message }}
                    </td>
                </tr>
                <tr class="subtitle">
                    <td colspan="12">
                        Generado y enviado por <b><a href="https://secuenciadigital.com">Secuencia</a></b>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
