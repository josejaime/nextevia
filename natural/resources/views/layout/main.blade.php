<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile First :: Disable the zooming capabilities in mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
 <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
 <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Natural Fit | @yield('title')</title>

    <meta name="description"
        content="100% Natural. 0 Azúcar, Más Stevia. La único con 100% Stevia Mexicana desde el Cultivo y Elaboración.">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/owl/dist/assets/owl.carousel.min.css?v=1.0.0') }}">
    {{-- <link rel="preload" href="{{ asset('lib/owl/dist/assets/owl.carousel.min.css?v=1.0.0') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{ asset('lib/owl/dist/assets/owl.carousel.min.css?v=1.0.0') }}">
    </noscript> --}}

    <link rel="stylesheet" type="text/css"
        href="{{ asset('lib/owl/dist/assets/owl.theme.default.min.css?v=1.0.0') }}">
    {{-- <link rel="preload" href="{{ asset('lib/owl/dist/assets/owl.theme.default.min.css?v=1.0.0') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{ asset('lib/owl/dist/assets/owl.theme.default.min.css?v=1.0.0') }}">
    </noscript> --}}

    <link rel="stylesheet" type="text/css" href="{{ asset('lib/semantic/semantic.min.css') }}">
    {{-- <link rel="preload" href="{{ asset('lib/semantic/semantic.min.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{ asset('lib/semantic/semantic.min.css') }}">
    </noscript> --}}

    <link href="{{ asset('css/main.css?v=2.8') }})" rel="stylesheet">
    <link href="{{ asset('css/patches.css?v=2.8') }})" rel="stylesheet">


    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/png">
    <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/png">

    <!-- Browser Toolbar Color -->
    <meta name="theme-color" content="#6ea485">

    <!-- Sharing metas -->
    <meta property="og:title" content="Natural Fit | @yield('title')">
    <meta property="og:description"
        content="100% Natural. 0 Azúcar, Más Stevia. La único con 100% Stevia Mexicana desde el Cultivo y Elaboración.">
    <meta property="og:image" content="{{ asset('img/natural_fit_logo.png') }}">
    <meta property="og:url" content="{!! Request::url() !!}">

    <meta name="twitter:title" content="Natural Fit | @yield('title')">
    <meta name="twitter:description"
        content="100% Natural. 0 Azúcar, Más Stevia. La único con 100% Stevia Mexicana desde el Cultivo y Elaboración.">
    <meta name="twitter:image" content="{{ asset('img/natural_fit_logo.png') }}">
    <meta name="twitter:card" content="summary_large_image">

    @section('head_styles')


        <!-- Facebook Pixel Code -->
        <script>
            ! function(f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function() {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '221259583476277');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
                src="https://www.facebook.com/tr?id=221259583476277&ev=PageView&noscript=1" /></noscript>
        <!-- End Meta Pixel Code -->


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156032522-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-156032522-1');
        </script>

        <!-- Google Tag Manager -->
        <script>
            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-WS6NLLX');
        </script>
        <!-- End Google Tag Manager -->

        <!-- Global site tag (gtag.js) - Google Ads: 815753133 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-815753133"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', 'AW-815753133');
        </script>

        <script src="https://www.google.com/recaptcha/api.js?render=6Lfza2MaAAAAACX9o5JvVMYpjADRnXisM4Mq9Ouf"></script>
    </head>

    <body>
        <div class="ui sidebar inverted vertical menu">
            <a class="item" href="{{ route('que_es') }}">¿Qué es?</a>
            <a class="item" href="{{ route('donde') }}">¿Dónde comprar?</a>
            <a class="item" href="{{ route('nutricion') }}">Nutrición y salud</a>
            <a class="item" href="{{ route('stevia') }}">Stevia</a>
            <a class="item" href="{{ route('contacto') }}">Contacto</a>
            <a class="item" href="{{ route('negocios') }}">Negocios</a>
            <a class="item" href="{{ route('productos') }}">Productos</a>
            <a class="item"
                href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=comprarahora"
                target="_blank" class="amazon">
                <img src="{{ asset('img/boton_amazon.png') }}" alt="">
            </a>
        </div>
        <div id="app" class="pusher">
            <!--<div class="preloader"><img src="{{ asset('img/seat_logo.svg') }}" alt="SEAT"></div>-->

            <header>
                <div class="nextevia">
                    <a href="#!">
                        <img src="{{ asset('img/nextevia_logo.png') }}" alt="Nextevia Logo" class="logo">
                    </a>
                </div>
                <div class="natural">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('img/natural_fit_logo.png') }}" alt="Natural Fit Logo" class="logo">
                    </a>
                    <div>
                        <a href="https://www.facebook.com/Natural-Fit-1653029765027422/" target="_blank"><i
                                class="fab fa-2x fa-facebook"></i></a>
                        <a href="https://www.instagram.com/stevianaturalfit/" target="_blank"><i
                                class="fab fa-2x fa-instagram"></i></a>
                        <a href="https://www.youtube.com/channel/UCLaasYewQ4LPU-YqeunGnWg" target="_blank"><i
                                class="fab fa-2x fa-youtube"></i></a>
                        {{-- <a href="https://twitter.com/naturalfitmx" target="_blank"><i class="fab fa-2x fa-twitter"></i></a> --}}
                    </div>
                </div>
            </header>

            <main>
                <nav class="menu">
                    <ul class="desktop">
                        <li><a href="{{ route('que_es') }}">¿Qué es?</a></li>
                        <li><a href="{{ route('donde') }}">¿Dónde comprar?</a></li>
                        <li><a href="{{ route('nutricion') }}">Nutrición y salud</a></li>
                        <li><a href="{{ route('stevia') }}">Stevia</a></li>
                        <li><a href="{{ route('contacto') }}">Contacto</a></li>
                        <li><a href="{{ route('negocios') }}">Negocios</a></li>

                        <li class="trigger">
                            <a href="{{ route('productos') }}">
                                Productos
                            </a>
                            <ul class="second">

                                <li><a class="productos" target="_blank"
                                        href="{{ route('productos') }}">Orgánica</a></li>
                                <li><a class="productos" target="_blank" href="{{ route('productos') }}">Hoja</a>
                                </li>
                                <li><a class="productos" target="_blank" href="{{ route('productos') }}">Natural
                                        Fit</a></li>
                                <li><a class="productos" target="_blank" href="{{ route('productos') }}">Stellar</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="https://www.amazon.com.mx/stores/page/31B6C393-0BE2-4DDA-80FA-A3CF9324E659?channel=comprarahora"
                                target="_blank" class="amazon button2">
                                <img src="{{ asset('img/boton_amazon_2.png') }}" alt="">
                            </a>

                        </li>
                    </ul>

                    <ul class="mobile">
                        <li>
                            <a href="#!" class="mobile-trigger">
                                <i class="fa fa-bars fa-2x"></i>
                            </a>
                        </li>
                    </ul>
                </nav>


                @yield('content')

            </main>
            <footer>
                <div class="natural">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('img/natural_fit_logo_blanco.png') }}" alt="Natural Fit Logo"
                            class="logo">
                    </a>
                    <div>
                        <a href="https://www.facebook.com/Natural-Fit-1653029765027422/"><i
                                class="fab fa-2x fa-facebook"></i></a>
                        <a href="https://www.instagram.com/stevianaturalfit/"><i class="fab fa-2x fa-instagram"></i></a>
                        <a href="https://www.youtube.com/channel/UCLaasYewQ4LPU-YqeunGnWg"><i
                                class="fab fa-2x fa-youtube"></i></a>
                        {{-- <a href="https://twitter.com/naturalfitmx"><i class="fab fa-2x fa-twitter"></i></a> --}}
                    </div>
                </div>
            </footer>
            {{-- <div class="preloader">
            <img src="{{ asset('img/natural_fit_logo.png') }}" alt="Natural Fit Logo" class="logo">
    </div> --}}
        </div>

        <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/fontawesome.css') }}">
        {{-- <link rel="preload" href="{{ asset('lib/fontawesome/css/fontawesome.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/fontawesome.css') }}">
    </noscript> --}}

        <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/brands.css') }}">
        {{-- <link rel="preload" href="{{ asset('lib/fontawesome/css/brands.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/brands.css') }}">
    </noscript> --}}

        <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/solid.css') }}">
        {{-- <link rel="preload" href="{{ asset('lib/fontawesome/css/solid.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/solid.css') }}">
    </noscript> --}}

        <link rel="stylesheet" href="{{ asset('lib/aos/dist/aos.css?v=1.0.0') }}">
        {{-- <link rel="preload" href="{{ asset('lib/aos/dist/aos.css?v=1.0.0') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{ asset('lib/aos/dist/aos.css?v=1.0.0') }}">
    </noscript> --}}


        <script src="{{ asset('lib/jquery/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('lib/owl/dist/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('lib/aos/dist/aos.js?v=1.0.0') }}"></script>
        {{-- <script src="{{ asset('lib/semantic/semantic.min.js') }}"></script> --}}
        <script src="{{ asset('lib/semantic/components/sidebar.min.js') }}"></script>
        <script src="{{ asset('lib/semantic/components/dimmer.min.js') }}"></script>
        <script src="{{ asset('lib/semantic/components/transition.min.js') }}"></script>
        <script src="{{ asset('lib/semantic/components/modal.min.js') }}"></script>

        <!-- cdnjs -->
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
        {{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script> --}}
        <script type="text/javascript">
            jQuery(function($) {

                /*$(window).on('load', function() {
                    setTimeout(function() {
                        $('.preloader').fadeOut();
                    }, 1500);
                });

                /// If it resists to disapear
                setTimeout(function() {
                    /// Force it
                    $('.preloader').fadeOut();
                }, 4500);*/

            })
        </script>
        <script defer>
            $(document).ready(function() {

                $(".mobile-trigger").on('click', function() {
                    $('.ui.sidebar')
                        .sidebar('toggle');
                });

            });
        </script>

        @stack('scripts')
        @stack('styles')

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WS6NLLX" height="0" width="0"
                style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    </body>

    </html>
